terraform {
  backend "s3" {
    bucket = "jaimelekfc"
    key    = "terraform.tfstate"
    region = "eu-west-1"
  }
}