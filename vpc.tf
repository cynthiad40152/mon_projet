### Variables
variable region {
  default = "eu-west-1"
}

### Provider
provider "aws" {
  region = "${var.region}"
}

### Resources
resource "aws_vpc" "mon_vpc" {
  cidr_block = "172.23.0.0/16"

  tags {
    Name = "mon_vpc"
  }
}

resource "aws_subnet" "public" {
  vpc_id                  = "${aws_vpc.mon_vpc.id}"
  cidr_block              = "172.23.1.0/24"
  availability_zone       = "eu-west-1a"
  map_public_ip_on_launch = "true"

  tags {
    Name = "subnet_public"
  }
}

resource "aws_subnet" "prive" {
  vpc_id            = "${aws_vpc.mon_vpc.id}"
  cidr_block        = "172.23.10.0/24"
  availability_zone = "eu-west-1a"

  tags {
    Name = "subnet_prive"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.mon_vpc.id}"

  tags {
    Name = "mon_ig"
  }
}

resource "aws_route_table" "route_table_public" {
  vpc_id = "${aws_vpc.mon_vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
  }

  tags {
    Name = "ma_route_table"
  }
}

resource "aws_route_table_association" "rtap_public" {
  subnet_id      = "${aws_subnet.public.id}"
  route_table_id = "${aws_route_table.route_table_public.id}"
}
