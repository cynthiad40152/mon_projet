### Variables
variable ami {
  default = "ami-08935252a36e25f85"
}

### EC2 Instances
resource "aws_instance" "instance1" {
  ami           = "${var.ami}"
  instance_type = "t2.micro"
  subnet_id     = "${aws_subnet.public.id}"
  key_name = "mykeypair"
  vpc_security_group_ids = ["${aws_security_group.sg.id}"]

  tags = {
    Name = "Mon_instance"
  }
}
