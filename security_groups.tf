resource "aws_security_group" "sg" {
  name        = "sg_monprojet"
  description = "sg communication"
  vpc_id      = "${aws_vpc.mon_vpc.id}"

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks     = ["0.0.0.0/0"]

  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks     = ["0.0.0.0/0"]

  }
}
